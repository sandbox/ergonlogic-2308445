<?php
/**
 * @file
 *   Vagrant service implementation for the hosting front end.
 */

/**
 * The base vagrant service type class.
 */
class hostingService_vagrant extends hostingService {
  public $service = 'vagrant';
}

/**
 * A Vagrant implementation of the cloud service type.
 */
class hostingService_cloud_vagrant extends hostingService_cloud {
  /**
   * node operations
   */

  /**
   * Load the reference to the associated Vagrant Config entity.
   */
  function load() {
    // REMEMBER TO CALL THE PARENT!
    parent::load();

    $this->mergeData("SELECT vagrantfile FROM {hosting_vagrant} WHERE server = :nid", array(':nid' => $this->server->nid));
    $this->vagrantfile = vagrantfile_load($this->vagrantfile);
  }

  /**
   * Display settings for adding the cloud element to the server node page.
   *
   * @param
   *   A reference to the associative array of the subsection of the page
   *   reserved for this service implementation.
   */
  function view(&$render) {
    // REMEMBER TO CALL THE PARENT!
    parent::view($render);
    $render['title']['#title'] = 'Vagrantfile type';
    $render['title']['#markup'] = _hosting_vagrantfile_type_link($this->type);
  }

  /**
   * Insert a record into the database.
   *
   * Create a vagrantfile and associate it to the server.
   *
   * @see: hosting_server_hook_insert().
   */
  function insert() {
    // REMEMBER TO CALL THE PARENT!
    parent::insert();
    $label = isset($this->server->human_name) ? $this->server->human_name : $this->server->title;
    $vf = vagrantfile_create($this->server->title, $label, $this->type);
    $vf->save();
    $id = db_insert('hosting_vagrant')
      ->fields(array(
        'server' => $this->server->nid,
        'vagrantfile' => $vf->name,
      ))
      ->execute();
  }

  /**
   * Delete a record from the database, based on server node.
   */
  function delete() {
    // REMEMBER TO CALL THE PARENT!
    parent::delete();
    $this->vagrantfile->delete();
    $id = db_delete('hosting_vagrant')
      ->condition('server', $this->server->nid)
      ->execute();
  }

}

/**
 * A pirate implementation of the vagrant service type.
 */
class hostingService_vagrant_pirate extends hostingService_vagrant {

  public $type = 'pirate';

  public $has_port = FALSE;

  public $has_restart_cmd = FALSE;

  /**
   * node operations
   */

  /**
   * Load associated values for the service.
   *
   * In this vagrant we will use the variable system to retrieve values.
   */
  function load() {
    // REMEMBER TO CALL THE PARENT!
    parent::load();

    $this->mergeData("SELECT vagrant_root FROM {hosting_vagrant_pirate} WHERE server = :nid", array(':nid' => $this->server->nid));
  }

  /**
   * Display settings on the server node page.
   *
   * @param
   *   A reference to the associative array of the subsection of the page
   *   reserved for this service implementation.
   */
  function view(&$render) {
    // REMEMBER TO CALL THE PARENT!
    parent::view($render);
    $render['vagrant_root'] = array(
      '#type' => 'item',
      '#title' => t('Vagrant root'),
      '#markup' => filter_xss($this->vagrant_root),
    );
  }

  /**
   * Extend the server node form.
   *
   * @param
   *   A reference to the associative array of the subsection of the form
   *   reserved for this service implementation.
   */
  function form(&$form) {
    // REMEMBER TO CALL THE PARENT!
    parent::form($form);

    $form['vagrant_root'] = array(
      '#type' => 'textfield',
      '#title' => t('Vagrant root'),
      '#description' => t('The filesystem path at which to build Vagrant projects (i.e., clouds).'),
      '#size' => 40,
      '#default_value' => isset($this->vagrant_root) ? $this->vagrant_root : '/var/aegir/clouds',
#      '#maxlength' => 64,
      '#weight' => 5,
    );
  }

  /**
   * Validate a form submission.
   */
  function validate(&$node, &$form) {
    // REMEMBER TO CALL THE PARENT!
    parent::validate($node, $form);
#check that it's a valid path
#    if (sizeof($this->vagrant_field) > 30) {
#      form_set_error('vagrant_field', t("The vagrant string must not be longer than 30 characters"));
#    }
  }

  /**
   * Insert a record into the database.
   *
   * Called by hosting_server_hook_insert().
   *
   * The values associated with this implementation have already
   * been set as properties of $this object, so we now need to
   * save them.
   */
  function insert() {
    // REMEMBER TO CALL THE PARENT!
    parent::insert();
    $id = db_insert('hosting_vagrant_pirate')
      ->fields(array(
        'server' => $this->server->nid,
        'vagrant_root' => $this->vagrant_root,
      ))
      ->execute();
  }

  /**
   * Update a record in the database.
   *
   * Called by hosting_server_hook_update().
   */
  function update() {
    // REMEMBER TO CALL THE PARENT!
    parent::update();
    $id = db_update('hosting_vagrant_update')
      ->fields(array(
        'vagrant_root' => $this->vagrant_root,
      ))
      ->condition('server', $this->server->nid)
      ->execute();
  }

  /**
   * Delete a record from the database, based on server node.
   */
  function delete() {
    // REMEMBER TO CALL THE PARENT!
    parent::delete();
    $id = db_insert('hosting_vagrant_pirate')
      ->condition('server', $this->server->nid)
      ->execute();
  }

  /**
   * Delete a specific revision from the database.
   *
   * Not relevant in our vagrant but shown anyway.
   */
  function delete_revision() {
    // REMEMBER TO CALL THE PARENT!
    parent::delete_revision();
  }

  /**
   * Pass values to the provision backend when we call provision-save.
   *
   * By selecting this type we already pass the '--vagrant_service_type=pirate' option
   * to the command, which will load the matching provisionService class in the backend.
   *
   * This backend class will be responsible for receiving and reacting to the options
   * passed here.
   *
   * @ingroup backend-frontend-IPC
   */
  public function context_options($task_type, $ref_type, &$task) {
    // REMEMBER TO CALL THE PARENT!
    parent::context_options($task_type, $ref_type, $task);

    $task->context_options['vagrant_root'] = $this->vagrant_root;
  }
}

