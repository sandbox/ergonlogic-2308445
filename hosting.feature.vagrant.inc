<?php

/**
 * @file
 *   Expose the vagrant feature to hostmaster.
 */

/**
 * Implements hook_hosting_feature().
 *
 * Register the vagrant hosting feature with Aegir, initially this feature will
 * be disabled.
 */
function hosting_vagrant_hosting_feature() {
  $features['vagrant'] = array(
    'title' => t('Vagrant service'),
    'description' => t('Provision VMs and containers using Vagrant.'),
    'status' => HOSTING_FEATURE_DISABLED,
    'module' => 'hosting_vagrant',
    'group' => 'experimental'
  );
  return $features;
}
