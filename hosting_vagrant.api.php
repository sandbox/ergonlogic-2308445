<?php

/**
 * Tell Hosting Vagrant about Vagrantfile types to exclude from service
 * class generation. Our module will have to define service classes in the
 * normal fashion, in hook_hosting_services(), in order to work.
 */
function hook_exclude_vagrantfile_types() {
  return array('xen');
}
